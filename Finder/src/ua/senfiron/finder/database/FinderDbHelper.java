package ua.senfiron.finder.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class FinderDbHelper extends SQLiteOpenHelper {

    public FinderDbHelper(Context context) {
        super(context, FinderContract.DATABASE_NAME, null, FinderContract.DATABASE_VERSION);
    }

    public void onCreate(SQLiteDatabase db) {
        db.execSQL(FinderContract.SearchEngine.SQL_QUERY_CREATE);
        db.execSQL(FinderContract.SearchResult.SQL_QUERY_CREATE);
        db.execSQL(FinderContract.Keywords.SQL_QUERY_CREATE);
    }

    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(FinderContract.SearchEngine.SQL_QUERY_DELETE);
        db.execSQL(FinderContract.SearchResult.SQL_QUERY_DELETE);
        db.execSQL(FinderContract.Keywords.SQL_QUERY_DELETE);
        onCreate(db);
    }

    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        onUpgrade(db, oldVersion, newVersion);
    }
}