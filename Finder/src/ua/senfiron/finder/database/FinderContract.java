package ua.senfiron.finder.database;

import android.provider.BaseColumns;

public class FinderContract {
    

    public static final int DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = "Finder.db";
    
    private static final String COMMA_SEP = ",";

    public static abstract class SearchEngine implements BaseColumns {
        public static final String TABLE_NAME = "SearchEngine";
        public static final String COLUMN_ENGINE_NAME = "EngineName";
        
        public static final String SQL_QUERY_CREATE = "CREATE TABLE "
                + TABLE_NAME + " ("
                + _ID + " INTEGER PRIMARY KEY" + COMMA_SEP
                + COLUMN_ENGINE_NAME + " TEXT" + COMMA_SEP 
                + " UNIQUE (" + COLUMN_ENGINE_NAME + ") ON CONFLICT IGNORE)";
        
        public static final String SQL_QUERY_DELETE = "DROP TABLE IF EXISTS " + TABLE_NAME;
        private SearchEngine() {}
    }

    public static abstract class SearchResult implements BaseColumns {
        public static final String TABLE_NAME = "SearchResult";
        public static final String COLUMN_TITLE = "Title";
        public static final String COLUMN_URL = "URL";
        public static final String COLUMN_ENGINE_ID = "SearchEngineID";
        public static final String COLUMN_KEYOWORDS_ID = "KeywordsID";
        
        public static final String SQL_QUERY_CREATE = "CREATE TABLE "
                + TABLE_NAME + " ("
                + _ID + " INTEGER PRIMARY KEY" + COMMA_SEP
                + COLUMN_TITLE + " TEXT" + COMMA_SEP
                + COLUMN_URL + " TEXT" + COMMA_SEP
                + COLUMN_ENGINE_ID + " INTEGER" + COMMA_SEP 
                + COLUMN_KEYOWORDS_ID + " INTEGER" 
                + " )";
        public static final String SQL_QUERY_DELETE = "DROP TABLE IF EXISTS " + TABLE_NAME;
        private SearchResult() {}
   }

    public static abstract class Keywords implements BaseColumns {
        public static final String TABLE_NAME = "Keywords";
        public static final String COLUMN_KEYWORDS = "Keywords";
        public static final String SQL_QUERY_CREATE = "CREATE TABLE "
                + TABLE_NAME + " ("
                + _ID + " INTEGER PRIMARY KEY" + COMMA_SEP
                + COLUMN_KEYWORDS + " TEXT "
                + " )";
        public static final String SQL_QUERY_DELETE = "DROP TABLE IF EXISTS " + TABLE_NAME;
        private Keywords() {}
    }

    private FinderContract() {}
}
