package ua.senfiron.finder;

public class SearchResultItem {
    private String mTitle;
    private String mUrl;
    private String mEngineName;

    private int mIconResId;

    public SearchResultItem(String title, String url, String engineName, int iconResId) {
        this.mTitle = title;
        this.mUrl = url;
        this.mEngineName = engineName;
        this.mIconResId = iconResId;
    }
    
    public String getTitle() {
        return mTitle;
    }
    public void setTitle(String title) {
        this.mTitle = title;
    }
    public String getUrl() {
        return mUrl;
    }
    public void setUrl(String url) {
        this.mUrl = url;
    }
    public String getEngineName() {
        return mEngineName;
    }

    public void setEngineName(String engineName) {
        this.mEngineName = engineName;
    }
    public int getIconResId() {
        return mIconResId;
    }
    public void setIconResId(int iconResId) {
        this.mIconResId = iconResId;
    }
}
