package ua.senfiron.finder.searchengine;

import android.os.AsyncTask;
import ua.senfiron.finder.SearchResultItem;

public abstract class AbstractSearchEngine {

    public static interface SearchEngineListener
    {
        public void onSerachResultLoaded(SearchResultItem item);

        public void onMaxResultsCountChanged(int count);
    }

    protected String mQuery = "";
    protected SearchEngineListener mListener;
    protected AsyncTask<?, ?, ?> mExecutor;
    
    public void setListener(SearchEngineListener listener)
    {
        this.mListener = listener;
    }

    public String getQuery() {
        return mQuery;
    }

    public void setQuery(String query) {
        this.mQuery = query;
    }

    public void setExecutor(AsyncTask<?, ?, ?> executor)
    {
        this.mExecutor = executor;
    }
    
    public boolean isCanceled()
    {
        if(mExecutor != null)
        {
            return mExecutor.isCancelled();
        }
        return false;
    }
    
    public abstract String getEngineName();
    public abstract int getIconResID();
    public abstract int loadResults(int maxCount);
    

}
