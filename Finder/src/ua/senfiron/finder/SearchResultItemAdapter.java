package ua.senfiron.finder;

import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class SearchResultItemAdapter extends BaseAdapter  {
    
    private final List<SearchResultItem> mItems;

    public SearchResultItemAdapter(final List<SearchResultItem> items) {
        this.mItems = items;
    }

    @Override
    public int getCount() {
        return this.mItems.size();
    }

    @Override
    public Object getItem(int position) {
        return this.mItems.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        
        View itemView = null;
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) parent.getContext()
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            itemView = inflater.inflate(R.layout.search_result_item, null);
        } else {
            itemView = convertView;
        }

        final SearchResultItem item = this.mItems.get(position);

        TextView textViewTitle = (TextView) itemView.findViewById(R.id.textViewSearchResultTitle);
        textViewTitle.setText(item.getTitle());

        ImageView imageViewEngineIcon = (ImageView) itemView.findViewById(R.id.imageViewEngineIcon);
        imageViewEngineIcon
                .setImageResource(item.getIconResId());

        return itemView;
    }

}
